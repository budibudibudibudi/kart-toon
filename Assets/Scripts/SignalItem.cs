﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SignalItem : MonoBehaviour
{
    public GameObject posisiAwal;
    public GameObject TargetSignal;
    //public float smoothSpeed;
    //public float Speed;
    //public Vector3 offset;
    // Start is called before the first frame update

    public GameObject Icon;
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        TargetSignal = GameObject.FindGameObjectWithTag("TargetSignal");
        
        /*
        Vector3 desiredPosition = target.transform.position + offset;
        Vector3 smoothedPosition = Vector3.Lerp(transform.position, desiredPosition, smoothSpeed);
        transform.position = smoothedPosition;
        */
        
    }
    public void DapatItem(){
        transform.position = TargetSignal.transform.position;
        gameObject.transform.parent = TargetSignal.transform;
        Icon.GetComponent<Image>().enabled = true;
    }

    public void ItemKosong(){
        transform.position = posisiAwal.transform.position;
        gameObject.transform.parent = posisiAwal.transform;
        Icon.GetComponent<Image>().enabled = false;
    }


}
