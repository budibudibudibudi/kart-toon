using UnityEngine;
[CreateAssetMenu(fileName = "Character", menuName = "Character")]
public class CharacterClass : ScriptableObject
{
    public string nama;
    public int harga;
    public bool isunlocked;
    public enum TYPE { COMMON,RARE,EPIC,LEGENDARY}
    public TYPE type;
    public float Acceleration;
    public float topSpeed;
    public float handling;
    public float toughness;
    public GameObject prefab;

}

public class CharacterBehaviour : MonoBehaviour
{
    private CharacterClass CC;
    //private void Start()
    //{
    //    CC = GetComponent<CharacterClass>();
    //}
}
