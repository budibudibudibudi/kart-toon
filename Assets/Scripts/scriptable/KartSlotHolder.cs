using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace KartToon
{
    public class KartSlotHolder : MonoBehaviour
    {
        public CharacterClass[] Karts;
        public static KartSlotHolder instance;
        private void Awake()
        {
            if (instance == null)
                instance = this;
            else
                Destroy(gameObject);

        }
    }

}
