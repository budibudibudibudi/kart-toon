﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Track : MonoBehaviour
{
    public bool available;

    public GameObject PlayLockedButton;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if(available == true){
            PlayLockedButton.SetActive(false);
        }
        if(available == false){
            PlayLockedButton.SetActive(true);
        }
    }
}
