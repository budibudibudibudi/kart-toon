﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class CoinSelectKart : MonoBehaviour
{
    public int Coin, HargaKart, Diamond;
    public Text CoinText, DiamondText;

    public GameObject LockObject, BuyButton;

    public int UnlockKart;

    public bool boolUnlockKart;

    public string kodeKart;

    public GameObject SelectColorPanel;

    public GameObject canvasKart;
    public GameObject kartSelectScript;
    //public bool isSelectionKart;
    
    public GameObject warningPanel;
    public GameObject iconKart, PriceCoin;
    
    // Start is called before the first frame update
    void Start()
    {
        Coin = PlayerPrefs.GetInt("Coin");
        Diamond = PlayerPrefs.GetInt("Diamond");
        CoinText.text = PlayerPrefs.GetInt("Coin", 0).ToString();
        DiamondText.text = PlayerPrefs.GetInt("Diamond", 0).ToString();

        UnlockKart = PlayerPrefs.GetInt(kodeKart, 1);
        //Debug.Log(kodeKart);
    }

    // Update is called once per frame
    void Update()
    {
        
        if (Input.GetKeyDown("r"))
        {
            PlayerPrefs.DeleteAll();
            SceneManager.LoadScene("MenuBase");
        }

        if (Input.GetKeyDown("="))
        {
            Coin += 5000;
            CoinText.text = Coin.ToString();
        }

        if (UnlockKart == 1){

            boolUnlockKart = false;
            LockObject.SetActive(true);
            BuyButton.SetActive(true);
            
        }
        if (UnlockKart == 2){

            boolUnlockKart = true;
            LockObject.SetActive(false);
            BuyButton.SetActive(false);
        }

        //if (kartSelectScript.GetComponent<CharacterSelection>().isSelectionKart == 1){
        //    canvasKart.SetActive(true);
        //}
        //if (kartSelectScript.GetComponent<CharacterSelection>().isSelectionKart == 0){
        //    canvasKart.SetActive(false);
        //}
        
    }

    public void BuyKart(){
        if (Coin >= HargaKart){
            Coin -= HargaKart;
            PlayerPrefs.SetInt("Coin", Coin);
            CoinText.text = Coin.ToString();
            UnlockKart = 1;
            iconKart.SetActive(true);
            PriceCoin.SetActive(false);
            if(UnlockKart == 1) // checks if you have the item
            {
                UnlockKart = 2;
                PlayerPrefs.SetInt(kodeKart, UnlockKart); // saves the gameobject
            }
        }
        if (Coin <= HargaKart){
            warningPanel.SetActive(true);
            StartCoroutine(nonAktifWarning());
        }
    }


    public void SelectColorOpen(){
        SelectColorPanel.SetActive(true);
    }

    public void SelectColorClose(){
        SelectColorPanel.SetActive(false);
    }

    IEnumerator nonAktifWarning(){
        yield return new WaitForSeconds(2f);
        warningPanel.SetActive(false);
    }
}
