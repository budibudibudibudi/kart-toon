﻿using KartToon.UI;
using Photon.Pun;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace KartToon.UI
{
    public class MainMenu : Page
    {
        [SerializeField] TrackClass[] tracks;
        [SerializeField] GameObject[] trackImage;
        [SerializeField] Image selectedTrack;
        [SerializeField] Sprite defaultImage;
        [SerializeField] GameObject connectingPanel;
        [SerializeField] Button singlePlayerBTN, multiPlayerBTN, playGameBTN;
        private void Start()
        {
            playGameBTN.onClick.AddListener(() => ChangeScene("KartTrack"));
            singlePlayerBTN.onClick.AddListener(SinglePlayer);
            multiPlayerBTN.onClick.AddListener(MultiPlayer);
            RefreshUI();
        }
        #region MultiPlayer Photon
        public void MultiPlayer()
        {
            PhotonNetwork.ConnectUsingSettings();
            connectingPanel.SetActive(true);
        }

        public override void OnConnectedToMaster()
        {
            PhotonNetwork.JoinLobby();
        }
        public override void OnJoinedLobby()
        {
            GameManager.Instance.ChangePage(PageName.MULTIPLAYERLOBBY);
            SceneManager.LoadScene("MultiplayerScene");
        }
        #endregion
        private void SinglePlayer()
        {
            TrackClass t = GameManager.Instance.GetTrack();
            if (t != null)
            {
                ChangeScene(t.nama);
            }
        }

        void RefreshUI()
        {
            TrackClass t = GameManager.Instance.GetTrack();
            if (t != null)
            {
                selectedTrack.sprite = t.trackImage;
            }
            else
                selectedTrack.sprite = tracks[0].trackImage;
            for (int i = 0; i < trackImage.Length; i++)
            {
                try
                {
                    trackImage[i].transform.GetChild(0).GetComponent<Image>().sprite = tracks[i].trackImage; //trackimage = 6
                    if(tracks[i].isUnlocked)
                    {
                        trackImage[i].transform.GetChild(1).gameObject.SetActive(false);
                    }
                    trackImage[i].transform.GetChild(2).GetComponent<Text>().text = tracks[i].nama;
                    connectingPanel.SetActive(false);
                }
                catch
                {
                    trackImage[0].transform.GetChild(0).GetComponent<Image>().sprite = defaultImage;
                    trackImage[i].transform.GetChild(1).gameObject.SetActive(true);
                    trackImage[i].transform.GetChild(2).GetComponent<Text>().text = "Coming Soon";
                    //Debug.Log("connectingPanel ga bisa ditutup");
                }
            }
        }
    }

}
