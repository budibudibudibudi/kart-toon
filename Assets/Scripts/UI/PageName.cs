namespace KartToon.UI
{
    public enum PageName
    {
        LOADING,
        MAINMENU,
        SINGLEPLAYER,
        GAMEPAUSED,
        GAMEUNPAUSED,
        MULTIPLAYER,
        COINSTORE,
        STORE,
        KART,
        EVENTS,
        SELECTLEVEL,
        PROFILE,
        MULTIPLAYERLOBBY,
        ROOMLIST
    }
}