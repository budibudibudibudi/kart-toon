using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace KartToon.UI
{
    public class ProfilePanel : Page
    {
        [SerializeField] Button closeBTN;
        private void Start()
        {
            closeBTN.onClick.AddListener(() => GameManager.Instance.ChangePage(PageName.MAINMENU));
        }
    }
}
