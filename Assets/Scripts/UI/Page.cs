using UnityEngine;
using UnityEngine.SceneManagement;
using Photon.Pun;

namespace KartToon.UI
{
    public class Page : MonoBehaviourPunCallbacks
    {
        [SerializeField] public PageName pageName;

        protected virtual void ChangeScene(string sceneName)
        {
            SceneManager.LoadScene(sceneName);
        }

    }

}
