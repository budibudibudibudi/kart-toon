using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

namespace KartToon.UI
{
    public class CanvasManager : MonoBehaviour
    {
        [SerializeField] protected Page[] allPages;
        [SerializeField] RotatorCamera rotateCamera;

        [SerializeField] TMP_Text coinText, gemText, coinBiruText;

        private void Start()
        {
            GameManager.Instance.OnStateChange += Instance_OnStateChange;
            GameManager.Instance.OnCurrenciesChange += OnCurrenciesChange;

        }

        private void OnCurrenciesChange(int coin, int gem,int coinBiru)
        {
            coinText.text =coin.ToString();
            gemText.text = gem.ToString();
            coinBiruText.text = coinBiru.ToString();
        }

        private void Instance_OnStateChange(PageName pageName)
        {
            switch (pageName)
            {
                case PageName.MAINMENU:
                    StartCoroutine(SetPage(pageName));
                    rotateCamera.SelectTrack();
                    break;
                case PageName.COINSTORE:
                    StartCoroutine(SetPage(pageName));
                    rotateCamera.SelectCoin();
                    break;
                case PageName.STORE:
                    StartCoroutine(SetPage(pageName));
                    rotateCamera.SelectShop();
                    break;
                case PageName.KART:
                    StartCoroutine(SetPage(pageName));
                    rotateCamera.SelectBase();
                    break;
                case PageName.EVENTS:
                    StartCoroutine(SetPage(pageName));
                    rotateCamera.SelectEvent();
                    break;
                case PageName.SELECTLEVEL:
                    StartCoroutine(SetPage(pageName));
                    break;
                default:
                    break;
            }
        }

        public virtual IEnumerator SetPage(PageName pageName)
        {
            foreach (var page in allPages)
            {
                page.gameObject.SetActive(false);
            }
            Page currentPage = Array.Find(allPages, p => p.pageName == pageName);
            yield return new WaitForSeconds(0.6f);
            currentPage?.gameObject.SetActive(true);

        }
    }
}
