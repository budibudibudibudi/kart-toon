﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace PowerslideKartPhysics
{
    public class CarCPManager : MonoBehaviour{

        public int CarNumber;
        public int cpCrossed = 0;

        public int CarPosition;

        public Kart scriptKart;

        public PlayerPosition playerPosition;

        public bool isHillTrack;

        public GameObject lastCPCollider;

        void Start(){
            
        }

        private void OnTriggerEnter(Collider other){
            if (other.gameObject.CompareTag("CP")){
                cpCrossed += 1;
                playerPosition.CarCollectedCp(CarNumber, cpCrossed);
                lastCPCollider = other.gameObject;
            }
        }
        void Update (){   
            if (isHillTrack == false){
                if (CarPosition == 1){
                    StartCoroutine(posisi1());
                }
                if (CarPosition == 2){
                    StartCoroutine(posisi2());
                }

                if (CarPosition == 3){
                    StartCoroutine(posisi3());
                }
            }

            if (isHillTrack == true){
                if (CarPosition == 1){
                    scriptKart.GetComponent<PowerslideKartPhysics.Kart>().maxSpeed = 50f;
                }
                if (CarPosition == 2){
                    scriptKart.GetComponent<PowerslideKartPhysics.Kart>().maxSpeed = 55f;
                }

                if (CarPosition == 3){
                    scriptKart.GetComponent<PowerslideKartPhysics.Kart>().maxSpeed = 60f;
                }
            }
        }
        IEnumerator posisi1(){
            yield return new WaitForSeconds(10f);
            scriptKart.GetComponent<PowerslideKartPhysics.Kart>().maxSpeed = 45f;
        }
        IEnumerator posisi2(){
            yield return new WaitForSeconds(20f);
            scriptKart.GetComponent<PowerslideKartPhysics.Kart>().maxSpeed = 60f;
        }
        IEnumerator posisi3(){
            yield return new WaitForSeconds(30f);
            scriptKart.GetComponent<PowerslideKartPhysics.Kart>().maxSpeed = 80f;
        }
        
    }

    
}