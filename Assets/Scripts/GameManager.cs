﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using KartToon.UI;
using Photon.Pun;

namespace KartToon
{
    public class GameManager : MonoBehaviour
    {
        public static GameManager Instance;
        [SerializeField]CharacterClass character;
        TrackClass track;

        [SerializeField] private int totalCoin;
        [SerializeField] private int totalGem;
        [SerializeField] private int totalCoinBiru;
        public delegate void OnCurrenciesValueChangeDelegate(int coin, int gem,int coinBiru);
        public OnCurrenciesValueChangeDelegate OnCurrenciesChange;

        [SerializeField] private PageName c_PageName;
        public delegate void StateChangeDelegate(PageName pageName);
        public event StateChangeDelegate OnStateChange;
        private void Awake()
        {
            if (Instance == null)
            {
                Instance = this;
            }
            else
            {
                Destroy(gameObject);
            }
            DontDestroyOnLoad(gameObject);
        }
        public void ChangePage(PageName newPage)
        {
            c_PageName = newPage;

            OnStateChange?.Invoke(c_PageName);
        }

        public void AddCurrenciesValue(int coin, int gem,int coinBiru)
        {
            totalCoin += coin;
            totalGem += gem;
            totalCoinBiru += coinBiru;

            OnCurrenciesChange?.Invoke(totalCoin, totalGem,totalCoinBiru);
        }
        public void setupCharacter(CharacterClass _character)
        {
            character = _character;
        }
        public CharacterClass GetCharacter()
        {
            return character;
        }
        public void SetTrack(TrackClass _track)
        {
            track = _track;
        }
        public TrackClass GetTrack()
        {
            return track;
        }
    }

}

