using UnityEngine;
using UnityEngine.UI;

namespace KartToon.UI
{
    public class MenuNew : MonoBehaviour
    {
        [SerializeField] private Button b_menu,b_store,b_coinStore,b_kart,b_event;

        private void Start()
        {
            b_menu.onClick.AddListener(() => GameManager.Instance.ChangePage(PageName.MAINMENU));
            b_store.onClick.AddListener(() => GameManager.Instance.ChangePage(PageName.STORE));
            b_coinStore.onClick.AddListener(() => GameManager.Instance.ChangePage(PageName.COINSTORE));
            b_kart.onClick.AddListener(() => GameManager.Instance.ChangePage(PageName.KART));
            b_event.onClick.AddListener(() => GameManager.Instance.ChangePage(PageName.EVENTS));

            GameManager.Instance?.AddCurrenciesValue(10, 10,10);
        }
    }
}