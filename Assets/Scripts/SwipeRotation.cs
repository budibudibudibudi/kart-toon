﻿using UnityEngine;

public class SwipeRotation : MonoBehaviour
{
    public float RotationSpeed = 5;
    public GameObject[] KartObject;
    public int i;
    // Update is called once per frame
    void OnMouseDrag () 
    {
        for(i = 0; i < KartObject.Length; i++){
            KartObject[i].GetComponent<Animator>().enabled = false;
        }

        float rotY = Input.GetAxis("Mouse X")*RotationSpeed*Mathf.Deg2Rad;
        transform.Rotate(Vector3.up, -rotY);
    }

    void OnMouseUp () 
    {
        for(i = 0; i < KartObject.Length; i++){
            KartObject[i].GetComponent<Animator>().enabled = true;
        }
    }
    private void Start()
    {
        
    }
}