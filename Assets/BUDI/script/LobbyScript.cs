using ExitGames.Client.Photon;
using KartToon.UI;
using Photon.Pun;
using Photon.Realtime;
using System;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace KartToon.Multiplayer
{
    public class LobbyScript : Page
    {
        [SerializeField] Button createRoom,confirmCreateRoom,openRoom;
        [SerializeField] TMP_InputField inputRoomName;
        [SerializeField] GameObject halaman1, halaman2;
        [SerializeField] GameObject content;
        [SerializeField] GameObject room;
        private void Start()
        {
            createRoom.onClick.AddListener(()=> {
                inputRoomName.gameObject.SetActive(true);
            });

            confirmCreateRoom.onClick.AddListener(Create);
            openRoom.onClick.AddListener(() =>
            {
                GameManager.Instance.ChangePage(PageName.ROOMLIST);
            });
        }
        private void Create()
        {
            RoomOptions roomOptions = new RoomOptions();
            roomOptions.MaxPlayers = 8;
            inputRoomName.gameObject.SetActive(false);
            //GameObject a = PhotonNetwork.InstantiateSceneObject(room.name,room.transform.position,room.transform.rotation);

            GameObject a = PhotonNetwork.Instantiate(room.name, room.transform.position, Quaternion.identity);
            a.transform.SetParent(content.transform);
            a.name = inputRoomName.text;
            a.GetComponentInChildren<TMP_Text>().text = inputRoomName.text;
            a.GetComponent<Button>().onClick.AddListener(() =>
            {
                PhotonNetwork.JoinRoom(a.name);
            });
            PhotonNetwork.CreateRoom(inputRoomName.text,roomOptions,null);
            GameManager.Instance.ChangePage(PageName.ROOMLIST);
        }
        
    }

}
