using Photon.Pun;
using KartToon.UI;
using Photon.Realtime;

namespace KartToon.Multiplayer
{
    public class RoomList : Page
    {
        public override void OnJoinedRoom()
        {
            GameManager.Instance.ChangePage(PageName.MULTIPLAYER);
            PhotonNetwork.LoadLevel(2);
        }
        public void JoinRoom()
        {
            RoomOptions roomOptions = new RoomOptions();
            roomOptions.MaxPlayers = 8;
            PhotonNetwork.JoinOrCreateRoom("a",roomOptions,null);
        }
    }
}