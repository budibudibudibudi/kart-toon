using Photon.Pun;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using System;
using ExitGames.Client.Photon;
using Photon.Realtime;

namespace KartToon.Multiplayer.UI
{
    public class StandbyMultiplayer :MonoBehaviourPunCallbacks 
    {
        PhotonView view;
        [SerializeField] private bool isReady;
        [SerializeField] GameObject ReadyUI;
        [SerializeField] Button readyBTN;
        private void Awake()
        {
            view = GetComponent<PhotonView>();
            if (!view.IsMine)
                Destroy(gameObject);
        }
        private void Start()
        {
            if(PhotonNetwork.IsMasterClient)
            {
                isReady = true;
                readyBTN.GetComponentInChildren<TMP_Text>().text = "StartGame";
                readyBTN.onClick.AddListener(StartGame);
                view.RPC("RefreshUI", RpcTarget.All);
            }
            else
            {
                isReady = false;
                view.RPC("RefreshUI", RpcTarget.All);
                readyBTN.GetComponentInChildren<TMP_Text>().text = "Ready";
                readyBTN.onClick.AddListener(Ready);
            }
            PhotonNetwork.NetworkingClient.EventReceived += OnEvent;
        }

        private void OnEvent(EventData obj)
        {
            if (obj.Code == 1)
            {
                //startgame;
            }
        }

        private void StartGame()
        {
            PhotonNetwork.RaiseEvent((byte)1, null, RaiseEventOptions.Default, SendOptions.SendReliable);
        }

        private void Ready()
        {
            if(!isReady)
            {
                isReady = true;
                readyBTN.GetComponent<Image>().color = Color.black;
            }
            else
            {
                isReady = false;
                readyBTN.GetComponent<Image>().color = Color.green;
            }
            view.RPC("RefreshUI", RpcTarget.All);
        }
        [PunRPC]
        public void RefreshUI()
        {
            if(isReady)
            {
                ReadyUI.GetComponent<Image>().color = Color.green;
                ReadyUI.GetComponentInChildren<TMP_Text>().text = "Ready";
            }
            else
            {
                ReadyUI.GetComponent<Image>().color = Color.black;
                ReadyUI.GetComponentInChildren<TMP_Text>().text = "Not Ready";
            }
        }
    }

}
